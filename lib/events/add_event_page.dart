import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:water_reminder/model/waterdetail.dart';

class AddEventPage extends StatefulWidget {
  @override
  _AddEventPageState createState() => _AddEventPageState();
}

class _AddEventPageState extends State<AddEventPage> {
  String _email = '';
  final _fromkey = GlobalKey<FormBuilderState>();
  Waterdetail mywaterdetail = Waterdetail(
      drink_name: '', drink_milliliter: '', drink_detail: '', drink_date: '');
  final Future<FirebaseApp> firebase = Firebase.initializeApp();
  CollectionReference _waterdatails =
      FirebaseFirestore.instance.collection('waterdatails');

  @override
  void initState() {
    setState(() {
      _email = FirebaseAuth.instance.currentUser!.email!;
    });
    super.initState();
  }

  // Future<void> addWaterdetails() {
  //   return waterdatails
  //       .add({
  //         'drink_name': 'Coffee',
  //         'milliliter': 300,
  //         'drink_detail': 'อเมริโก่โน่',
  //       })
  //       .then((value) => print('Water detail'))
  //       .catchError((error) => print('Failed to add waterdetail: $error'));
  // }

  double value = 75;
  double _currentSliderValue = 20;

  @override
  Widget build(BuildContext context) {
    final double min = 0;
    final double max = 1000;
    return FutureBuilder(
        future: firebase,
        builder: (context, snapshot) {
          if (snapshot.hasError) {
            return Scaffold(
              appBar: AppBar(
                title: Text('Error'),
              ),
              body: Center(
                child: Text("${snapshot.error}"),
              ),
            );
          } else if (snapshot.connectionState == ConnectionState.done) {
            return Scaffold(
              appBar: AppBar(
                title: Text('เพิ่มข้อมูลการดื่มน้ำ'),
              ),
              body: Form(
                key: _fromkey,
                child: SingleChildScrollView(
                  child: Padding(
                    padding: const EdgeInsets.all(20.0),
                    child: Column(
                      children: [
                        TextFormField(
                          decoration: InputDecoration(
                            hintText: "ชื่อเครื่องดื่ม",
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(20)),
                            prefixIcon: Icon(Icons.coffee_rounded),
                          ),
                          validator: (drinkname) {
                            if (drinkname == null || drinkname.isEmpty) {
                              return 'โปรดกรอกชื่อเครื่องดื่ม';
                            }
                          },
                          onSaved: (drinkname) {
                            mywaterdetail.drink_name = drinkname!;
                          },
                        ),
                        SizedBox(height: 10),
                        Text("เพิ่มปริมาณน้ำที่คุณดื่ม (มิลลิลิตร)",
                            style: TextStyle(
                                fontSize: 16,
                                fontWeight: FontWeight.bold,
                                color: Colors.black54)),
                        SizedBox(height: 10),
                        SizedBox(height: 10),
                        SliderTheme(
                          data: SliderThemeData(
                            trackHeight: 80,
                            thumbShape: SliderComponentShape.noOverlay,
                            overlayShape: SliderComponentShape.noOverlay,
                            valueIndicatorShape: SliderComponentShape.noOverlay,
                            trackShape: RectangularSliderTrackShape(),

                            /// ticks in between
                            activeTickMarkColor: Colors.transparent,
                            inactiveTickMarkColor: Colors.transparent,
                          ),
                          child: Container(
                            height: 300,
                            child: Column(
                              children: [
                                buildSideLabel(max),
                                const SizedBox(height: 16),
                                Expanded(
                                  child: Stack(
                                    children: [
                                      Image.asset(
                                          "assets/icons/bottleicon.png"),
                                      RotatedBox(
                                        quarterTurns: 3,
                                        child: Slider(
                                          value: value,
                                          min: min,
                                          max: max,
                                          divisions: 20,
                                          label: value.round().toString(),
                                          onChanged: (value) => setState(
                                              () => this.value = value),
                                        ),
                                      ),
                                      Center(
                                        child: Text(
                                          '${value.round()}',
                                          style: TextStyle(
                                            color: Colors.white,
                                            fontWeight: FontWeight.bold,
                                            fontSize: 24,
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                const SizedBox(height: 16),
                                buildSideLabel(min),
                              ],
                            ),
                          ),
                        ),
                        SizedBox(height: 10),
                        TextFormField(
                          decoration: InputDecoration(
                            hintText: "รายละเอียด",
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(20)),
                            prefixIcon: Icon(Icons.library_books),
                          ),
                        ),
                        SizedBox(height: 10),
                        FormBuilderDateTimePicker(
                          decoration: InputDecoration(
                            hintText: "วันที่",
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(20)),
                            prefixIcon: Icon(Icons.calendar_today),
                          ),
                          name: "date",
                          initialValue: DateTime.now(),
                          fieldHintText: "เพิ่ม วัน/เดือน/ปี",
                          inputType: InputType.date,
                        ),
                        SizedBox(height: 10),
                        SizedBox(
                            width: double.infinity,
                            child: ElevatedButton(
                                child: Text(
                                  "บันทึกข้อมูล",
                                  style: TextStyle(fontSize: 20.0),
                                ),
                                style: ElevatedButton.styleFrom(
                                    primary: Colors.blue,
                                    fixedSize: const Size(50, 50),
                                    shape: RoundedRectangleBorder(
                                        borderRadius:
                                            BorderRadius.circular(50))),
                                onPressed: () async {
                                  if (_fromkey.currentState!.validate()) {
                                    _fromkey.currentState!.save();
                                    await _waterdatails.add({
                                      "drink_name": mywaterdetail.drink_name,
                                      "drink_milliliter":
                                          mywaterdetail.drink_milliliter,
                                      "drink_detail":
                                          mywaterdetail.drink_detail,
                                      "drink_date": mywaterdetail.drink_date
                                    });
                                    _fromkey.currentState!.reset();
                                  }
                                })),
                      ],
                    ),
                  ),
                ),
              ),
            );
          }
          return Scaffold(body: Center(child: CircularProgressIndicator()));
        });
  }

  Widget buildSideLabel(double value) => Text(
        value.round().toString(),
        style: TextStyle(fontSize: 24, fontWeight: FontWeight.bold),
      );
}
