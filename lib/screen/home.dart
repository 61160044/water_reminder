import 'package:flutter/material.dart';
import 'package:water_reminder/screen/login.dart';
import 'package:water_reminder/screen/register.dart';

class Homescreen extends StatelessWidget {
  const Homescreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Register/Login"),
        ),
        body: Padding(
            padding: const EdgeInsets.fromLTRB(10, 20, 10, 20),
            child: SingleChildScrollView(
              child: Column(
                children: [
                  Image.asset("assets/images/WaterIcon.png"),
                  SizedBox(
                    width: double.infinity,
                    child: ElevatedButton.icon(
                        onPressed: () {
                          Navigator.pushReplacement(context,
                              MaterialPageRoute(builder: (context) {
                            return RegisterScreen();
                          })
                          );
                        },
                        icon: Icon(Icons.add),
                        label: Text(
                          "สร้างบัญชีผู้ใช้",
                          style: TextStyle(fontSize: 20),
                        )),
                  ),
                  SizedBox(
                    width: double.infinity,
                    child: ElevatedButton.icon(
                        onPressed: () {
                          Navigator.pushReplacement(context,
                              MaterialPageRoute(builder: (context) {
                            return LoginScreen();
                          }));
                        },
                        icon: Icon(Icons.login),
                        label: Text(
                          "เข้าสู่ระบบ",
                          style: TextStyle(fontSize: 20),
                        )),
                  )
                ],
              ),
            )));
  }
}
