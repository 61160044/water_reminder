import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:water_reminder/main.dart';
import 'package:water_reminder/model/profile.dart';
import 'package:email_validator/email_validator.dart';
import 'package:water_reminder/screen/home.dart';
import 'package:water_reminder/screen/login.dart';

class RegisterScreen extends StatefulWidget {
  RegisterScreen({Key? key}) : super(key: key);

  @override
  _RegisterScreenState createState() => _RegisterScreenState();
}

class _RegisterScreenState extends State<RegisterScreen> {
  final _formkey = GlobalKey<FormState>();
  Profile profile = Profile(email: '', password: '');
  final Future<FirebaseApp> firebase = Firebase.initializeApp();

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: firebase,
      builder: (context, snapsshot) {
        if (snapsshot.hasError) {
          return Text('Error..');
        } else if (snapsshot.connectionState == ConnectionState.done) {
          return Scaffold(
              appBar: AppBar(
                title: Text("สร้างบัญชีผู้ใช้"),
              ),
              body: Container(
                child: Padding(
                  padding: const EdgeInsets.all(15.0),
                  child: SingleChildScrollView(
                    child: Form(
                      key: _formkey,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          SizedBox(height: 30),
                          Text("สมัครใช้งานบัญชี",
                              style: TextStyle(
                                  fontSize: 24,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.blueAccent)),
                          SizedBox(height: 5),
                          Text("Water Reminder!",
                              style: TextStyle(
                                  fontSize: 24,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.blueAccent)),
                          SizedBox(height: 10),
                          Text("กรอกข้อมูลเพื่อดำเนินการต่อไป"),
                          SizedBox(height: 10),
                          TextFormField(
                            keyboardType: TextInputType.emailAddress,
                            decoration: InputDecoration(
                              hintText: 'อีเมล์',
                              border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(20)),
                              prefixIcon: Icon(Icons.email),
                            ),
                            validator: (email) {
                              if (email == null || email.isEmpty) {
                                return 'โปรดกรอกอีเมล์';
                              }
                              if (email != null &&
                                  !EmailValidator.validate(email)) {
                                return 'รูปแบบอีเมล์ไม่ถูกต้อง';
                              }
                            },
                            onSaved: (email) {
                              profile.email = email!;
                            },
                          ),
                          SizedBox(height: 15.0, width: double.infinity),
                          TextFormField(
                            obscureText: true,
                            decoration: InputDecoration(
                              hintText: 'รหัสผ่าน',
                              border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(20)),
                              prefixIcon: Icon(Icons.lock),
                            ),
                            autofillHints: [AutofillHints.password],
                            validator: (password) {
                              if (password == null || password.isEmpty) {
                                return 'โปรดกรอกรหัสผ่าน';
                              }
                              if (password.length < 6) {
                                return 'รหัสผ่านต้องมีความยาวกว่า 6 ตัวอักษร';
                              }
                            },
                            onSaved: (password) {
                              profile.password = password!;
                            },
                          ),
                          SizedBox(height: 15.0, width: double.infinity),
                          SizedBox(
                            width: double.infinity,
                            child: ElevatedButton(
                              child: Text(
                                "ลงทะเบียน",
                                style: TextStyle(fontSize: 20.0),
                              ),
                              style: ElevatedButton.styleFrom(
                                  primary: Colors.blue,
                                  fixedSize: const Size(50, 50),
                                  shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(50))),
                              onPressed: () async {
                                if (_formkey.currentState!.validate()) {
                                  _formkey.currentState!.save();
                                  try {
                                    await FirebaseAuth.instance
                                        .createUserWithEmailAndPassword(
                                            email: profile.email,
                                            password: profile.password)
                                        .then((value) {
                                      _formkey.currentState!.reset();
                                      ScaffoldMessenger.of(context)
                                        ..removeCurrentSnackBar()
                                        ..showSnackBar(SnackBar(
                                            content: Text(
                                                'สร้างบัญชีผู้ใช้เรียบร้อยแล้ว')));

                                      Navigator.pushReplacement(context,
                                          MaterialPageRoute(builder: (context) {
                                        return Homescreen();
                                      }));
                                    });
                                  } on FirebaseAuthException catch (e) {
                                    print(e.code);
                                    print(e.message);
                                    ScaffoldMessenger.of(context)
                                      ..removeCurrentSnackBar()
                                      ..showSnackBar(SnackBar(
                                          content: Text(
                                              'อีเมล์นี้มีผู้ใช้งานแล้ว')));
                                  }
                                }
                              },
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.symmetric(vertical: 10.0),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Text("มีบัญชีผู้ใช้อยู่แล้วใช่หรือไม่ ?"),
                                SizedBox(width: 5),
                                TextButton(
                                    onPressed: () {
                                      Navigator.pushReplacement(context,
                                          MaterialPageRoute(builder: (context) {
                                        return LoginScreen();
                                      }));
                                    },
                                    child: Text("เข้าสู่ระบบ"))
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ));
        }
        return Scaffold(body: Center(child: CircularProgressIndicator()));
      },
    );
  }
}
