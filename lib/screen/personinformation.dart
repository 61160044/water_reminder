import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:water_reminder/model/information.dart';
import 'package:water_reminder/screen/home.dart';
import 'package:water_reminder/screen/mainmenu.dart';

class PersonInformationScrren extends StatefulWidget {
  PersonInformationScrren({Key? key}) : super(key: key);

  @override
  _PersonInformationScrrenState createState() =>
      _PersonInformationScrrenState();
}

class _PersonInformationScrrenState extends State<PersonInformationScrren> {
  String gender = '-';
  String _email = '';
  final _formkey = GlobalKey<FormState>();
  Information myinformation = Information(
      fullname: '', nickname: '', gender: '', weight: '', height: '', age: '');
  final Future<FirebaseApp> firebase = Firebase.initializeApp();
  var currentUser = FirebaseAuth.instance.currentUser;
  CollectionReference _userinformationCollection =
      FirebaseFirestore.instance.collection("users");
  @override
  void initState() {
    setState(() {
      _email = FirebaseAuth.instance.currentUser!.email!;
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: firebase,
        builder: (context, snapshot) {
          if (snapshot.hasError) {
            return Scaffold(
              appBar: AppBar(
                title: Text('Error'),
              ),
              body: Center(
                child: Text("${snapshot.error}"),
              ),
            );
          } else if (snapshot.connectionState == ConnectionState.done) {
            return Scaffold(
                appBar: new AppBar(
                  title: Text("ข้อมูลส่วนบุคคล"),
                ),
                body: Container(
                  padding: EdgeInsets.all(20),
                  child: Form(
                    key: _formkey,
                    child: SingleChildScrollView(
                      child: Column(
                        children: [
                          TextField(
                            readOnly: true,
                            decoration: InputDecoration(
                                prefixIcon: Icon(Icons.email),
                                border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(10.0),
                                ),
                                filled: true,
                                hintStyle: TextStyle(color: Colors.grey[800]),
                                hintText: " ${_email}",
                                fillColor: Colors.white70),
                          ),
                          SizedBox(height: 10),
                          TextFormField(
                            decoration: InputDecoration(
                              hintText: 'ชื่อ-นามสกุล',
                              border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(20)),
                              prefixIcon: Icon(Icons.account_box),
                            ),
                            validator: (fullname) {
                              if (fullname == null || fullname.isEmpty) {
                                return 'โปรดกรอกชื่อและนามสกุล';
                              }
                              if (fullname.length < 6) {
                                return 'ชื่อและนามสกุลต้องมีควายาวมากกว่า 6 ตัวอักษร';
                              }
                            },
                            onSaved: (fullname) {
                              myinformation.fullname = fullname!;
                            },
                          ),
                          SizedBox(height: 10),
                          TextFormField(
                            decoration: InputDecoration(
                              hintText: 'ชื่อเล่น',
                              border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(20)),
                              prefixIcon: Icon(Icons.account_circle),
                            ),
                            validator: (nickname) {
                              if (nickname == null || nickname.isEmpty) {
                                return 'โปรดกรอกชื่อเล่น';
                              }
                            },
                            onSaved: (nickname) {
                              myinformation.nickname = nickname!;
                            },
                          ),
                          SizedBox(height: 10),
                          Container(
                              padding: EdgeInsets.symmetric(
                                  horizontal: 12, vertical: 4),
                              decoration: BoxDecoration(
                                  border:
                                      Border.all(color: Colors.grey, width: 1),
                                  borderRadius: BorderRadius.circular(20)),
                              child: DropdownButtonHideUnderline(
                                child: DropdownButton(
                                  style: new TextStyle(
                                    fontWeight: FontWeight.normal,
                                    color: Colors.black54,
                                    fontSize: 16,
                                  ),
                                  isExpanded: true,
                                  iconSize: 28,
                                  icon: Icon(
                                    Icons.arrow_drop_down_circle,
                                    color: Colors.grey,
                                  ),
                                  items: [
                                    DropdownMenuItem(
                                      child: Text('กรุณาเลือกเพศ'),
                                      value: '-',
                                    ),
                                    DropdownMenuItem(
                                      child: Text('ชาย'),
                                      value: 'ชาย',
                                    ),
                                    DropdownMenuItem(
                                      child: Text('หญิง'),
                                      value: 'หญิง',
                                    )
                                  ],
                                  value: gender,
                                  onChanged: (String? newValue) {
                                    setState(() {
                                      gender = newValue!;
                                    });
                                  },
                                ),
                              )),
                          SizedBox(height: 10),
                          TextFormField(
                            decoration: InputDecoration(
                              hintText: 'อายุ',
                              border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(20),
                              ),
                              prefixIcon: Icon(Icons.groups_rounded),
                            ),
                            keyboardType: TextInputType.number,
                            validator: (weight) {
                              if (weight == null || weight.isEmpty) {
                                return 'โปรดกรอกอายุของคุณ';
                              }
                            },
                            onSaved: (age) {
                              myinformation.age = age!;
                            },
                          ),
                          SizedBox(height: 10),
                          TextFormField(
                            decoration: InputDecoration(
                              hintText: 'น้ำหนัก (กิโลกรัม)',
                              border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(20),
                              ),
                              prefixIcon: Icon(Icons.monitor_weight),
                            ),
                            keyboardType: TextInputType.number,
                            validator: (weight) {
                              if (weight == null || weight.isEmpty) {
                                return 'โปรดกรอกน้ำหนักตัวของคุณ';
                              }
                            },
                            onSaved: (weight) {
                              myinformation.weight = weight!;
                            },
                          ),
                          SizedBox(height: 10),
                          TextFormField(
                            decoration: InputDecoration(
                              hintText: 'ส่วนสูง (เซนติเมตร)',
                              border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(20),
                              ),
                              prefixIcon: Icon(Icons.nature_people),
                            ),
                            keyboardType: TextInputType.number,
                            validator: (tall) {
                              if (tall == null || tall.isEmpty) {
                                return 'โปรดกรอกส่วนสูงของคุณ';
                              }
                            },
                            onSaved: (tall) {
                              myinformation.height = tall!;
                            },
                          ),
                          SizedBox(height: 10),
                          SizedBox(
                            width: double.infinity,
                            child: ElevatedButton(
                              child: Text(
                                "บันทึกข้อมูล",
                                style: TextStyle(fontSize: 20.0),
                              ),
                              style: ElevatedButton.styleFrom(
                                  primary: Colors.blue,
                                  fixedSize: const Size(50, 50),
                                  shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(50))),
                              onPressed: () async {
                                if (_formkey.currentState!.validate()) {
                                  _formkey.currentState!.save();
                                  await _userinformationCollection.add({
                                    "full_name": myinformation.fullname,
                                    "nickname": myinformation.nickname,
                                    "gender": myinformation.gender,
                                    "age": myinformation.age,
                                    "weight": myinformation.weight,
                                    "height": myinformation.height
                                  }
                                  );
                                  _formkey.currentState!.reset();
                                  // print("${myinformation.fullname}");
                                  // print("${myinformation.nickname}");
                                  // print("${myinformation.gender}");
                                  // print("${myinformation.height}");
                                  // print("${myinformation.weight}");
                                }
                              },
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ));
          }
          return Scaffold(body: Center(child: CircularProgressIndicator()));
        });
  }
}
