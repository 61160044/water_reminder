import 'package:email_validator/email_validator.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:water_reminder/model/profile.dart';
import 'package:water_reminder/screen/home.dart';
import 'package:water_reminder/screen/mainmenu.dart';
import 'package:water_reminder/screen/register.dart';

class LoginScreen extends StatefulWidget {
  LoginScreen({Key? key}) : super(key: key);

  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final _formkey = GlobalKey<FormState>();
  Profile profile = Profile(email: '', password: '');
  final Future<FirebaseApp> firebase = Firebase.initializeApp();

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: firebase,
      builder: (context, snapsshot) {
        if (snapsshot.hasError) {
          return Text('Error!!..');
        } else if (snapsshot.connectionState == ConnectionState.done) {
          return Scaffold(
              appBar: AppBar(
                title: Text("ลงชื่อเข้าใช้"),
              ),
              body: Container(
                child: Padding(
                  padding: const EdgeInsets.all(15.0),
                  child: SingleChildScrollView(
                    child: Form(
                      key: _formkey,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          SizedBox(height: 30),
                          Text("ยินดีต้อนรับเข้าสู่",
                              style: TextStyle(
                                  fontSize: 24,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.blueAccent)),
                          SizedBox(height: 5),
                          Text("Water Reminder!",
                              style: TextStyle(
                                  fontSize: 24,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.blueAccent)),
                          SizedBox(height: 10),
                          Text("เข้าสู่ระบบเพื่อดำเนินการต่อไป"),
                          SizedBox(height: 10),
                          TextFormField(
                            keyboardType: TextInputType.emailAddress,
                            decoration: InputDecoration(
                              hintText: 'อีเมล์',
                              border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(20)),
                              prefixIcon: Icon(Icons.email),
                            ),
                            validator: (email) {
                              if (email == null || email.isEmpty) {
                                return 'โปรดกรอกอีเมล์';
                              }
                              if (email != null &&
                                  !EmailValidator.validate(email)) {
                                return 'รูปแบบอีเมล์ไม่ถูกต้อง';
                              }
                            },
                            onSaved: (email) {
                              profile.email = email!;
                            },
                          ),
                          SizedBox(height: 15.0, width: double.infinity),
                          TextFormField(
                            obscureText: true,
                            decoration: InputDecoration(
                              hintText: 'รหัสผ่าน',
                              border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(20)),
                              prefixIcon: Icon(Icons.vpn_key),
                            ),
                            autofillHints: [AutofillHints.password],
                            validator: (password) {
                              if (password == null || password.isEmpty) {
                                return 'โปรดกรอกรหัสผ่าน';
                              }
                              if (password.length < 6) {
                                return 'รหัสผ่านต้องมีความยาวกว่า 6 ตัวอักษร';
                              }
                            },
                            onSaved: (password) {
                              profile.password = password!;
                            },
                          ),
                          SizedBox(height: 15.0, width: double.infinity),
                          SizedBox(
                            width: double.infinity,
                            child: ElevatedButton(
                              child: Text(
                                "เข้าสู่ระบบ",
                                style: TextStyle(fontSize: 20.0),
                              ),
                              style: ElevatedButton.styleFrom(
                                  primary: Colors.blue,
                                  fixedSize: const Size(50, 50),
                                  shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(50))),
                              onPressed: () async {
                                if (_formkey.currentState!.validate()) {
                                  _formkey.currentState!.save();
                                  try {
                                    await FirebaseAuth.instance
                                        .signInWithEmailAndPassword(
                                            email: profile.email,
                                            password: profile.password)
                                        .then((value) {
                                      _formkey.currentState!.reset();
                                      Navigator.pushReplacement(context,
                                          MaterialPageRoute(builder: (context) {
                                        return MainMenu();
                                      }));
                                    });
                                  } on FirebaseAuthException catch (e) {
                                    String message = '';
                                    if (e.code == 'wrong-password') {
                                      message = "รหัสผ่านไม่ถูกต้อง";
                                    } else if (e.code == 'user-not-found') {
                                      message = "ไม่มีอีเมล์ผู้ใช้งานนี้ในระบบ";
                                    } else if (e.code == 'too-many-requests') {
                                      message =
                                          "กรุณาอย่าส่งข้อมูลซ้ำมากเกินไป";
                                    } else {
                                      message = e.message!;
                                    }
                                    print(e.code);
                                    Fluttertoast.showToast(
                                        msg: message,
                                        gravity: ToastGravity.CENTER,
                                        backgroundColor: Colors.red);
                                  }
                                }
                              },
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.symmetric(vertical: 10.0),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Text("ไม่มีบัญชีผู้ใช้ใช่หรือไม่ ?"),
                                SizedBox(width: 5),
                                TextButton(
                                    onPressed: () {
                                      Navigator.pushReplacement(context,
                                          MaterialPageRoute(builder: (context) {
                                        return RegisterScreen();
                                      }));
                                    },
                                    child: Text("สมัครเลย"))
                              ],
                            ),
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Text("- เข้าสู่ระบบด้วยวิธีอื่น -"),
                              SizedBox(width: 5),
                            ],
                          ),
                          SizedBox(height: 15),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: <Widget>[
                              GestureDetector(
                                onTap: () => print('Login with Google'),
                                child: Container(
                                  height: 60.0,
                                  width: 60.0,
                                  decoration: BoxDecoration(
                                      shape: BoxShape.circle,
                                      color: Colors.white,
                                      boxShadow: [
                                        BoxShadow(
                                            color: Colors.black26,
                                            offset: Offset(0, 2),
                                            blurRadius: 6.0)
                                      ],
                                      image: DecorationImage(
                                          image: AssetImage(
                                              "assets/icons/google.png"))),
                                ),
                              ),
                              GestureDetector(
                                onTap: () => print('Login with Facebook'),
                                child: Container(
                                  height: 60.0,
                                  width: 60.0,
                                  decoration: BoxDecoration(
                                      shape: BoxShape.circle,
                                      color: Colors.white,
                                      boxShadow: [
                                        BoxShadow(
                                            color: Colors.black26,
                                            offset: Offset(0, 2),
                                            blurRadius: 6.0)
                                      ],
                                      image: DecorationImage(
                                          image: AssetImage(
                                              "assets/icons/facebook.png"))),
                                ),
                              ),
                              GestureDetector(
                                onTap: () => print('Login with Twitter'),
                                child: Container(
                                  height: 60.0,
                                  width: 60.0,
                                  decoration: BoxDecoration(
                                      shape: BoxShape.circle,
                                      color: Colors.white,
                                      boxShadow: [
                                        BoxShadow(
                                            color: Colors.black26,
                                            offset: Offset(0, 2),
                                            blurRadius: 6.0)
                                      ],
                                      image: DecorationImage(
                                          image: AssetImage(
                                              "assets/icons/twitter.png"))),
                                ),
                              )
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ));
        }
        return Scaffold(body: Center(child: CircularProgressIndicator()));
      },
    );
  }
}
