class Information {
  String fullname;
  String nickname;
  String gender;
  String weight;
  String height;
  String age;

  Information(
      {required this.fullname,
      required this.gender,
      required this.nickname,
      required this.height,
      required this.weight,
      required this.age
      });
}
