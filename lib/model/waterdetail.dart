class Waterdetail {
  String drink_date;
  String drink_detail;
  String drink_milliliter;
  String drink_name;

  Waterdetail({
    required this.drink_date,
    required this.drink_detail,
    required this.drink_milliliter,
    required this.drink_name,
  });
}
